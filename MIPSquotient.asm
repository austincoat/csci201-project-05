
# num holds the numerator, denom holds the denominator;
# calculate quotient of num / denom and place in quotient

.data
num:      .word 77
denom:    .word 9
quotient: .word 0

.text
.globl main

main:

        ld $t0,num
		ld $t1,denom
		div $t2,$t0,$t1
		sw $t2,quotient

        jr $ra