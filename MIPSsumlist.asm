# the variable count contains the number of
# bytes in the vals array; the number of bytes
# will be 4 * number of elements; so the last
# valid memory position in the array is count-4

# store the sum of the array in the variable sum
        
.data
count:  .word 20               
vals:   .word 1, 2, 3, 4, 5
sum:    .word 0
        
.text   
.globl main

main:
        la $t0,vals
		la $t4,vals
		lw $t1,count
		Z:
		lw $t2, ($t0)
		add $t3,$t3,$t2
		addi $t0,$t0,4
		sub $t5,$t0,$t4
		bne $t5,$t1,Z
		sw $t3,sum
        jr $ra                  