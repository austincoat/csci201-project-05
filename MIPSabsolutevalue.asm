
# val holds the original value, absolute value should
# be calculated and saved into absval

.data
val:    .word -10
absval: .word 0

.text
.globl main

main:

        
		
		lw $t6, val
		bge $t6, $zero, N
		addi $t5,$zero,-1
		mul $t4, $t6,$t5
		sw $t4, absval
		N:
		sw $t4, absval
        jr $ra