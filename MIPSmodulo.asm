
# num holds the numerator, denom holds the denominator;
# calculate remainder of num % denom and place in remainder

.data
num:       .word 77
denom:     .word 9
remainder: .word 0

.text
.globl main

main:

        
		ld $t0, num
		Subs:
		ld $t1, denom
		rem $t3,$t0,$t1
		sw $t3,remainder
		
		
		
		
		jr $ra